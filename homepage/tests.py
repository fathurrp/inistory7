from django.test import TestCase, LiveServerTestCase
from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage, redirecting
from django.http import HttpRequest

# Create your tests here.

class UnitTestStory6(TestCase):
	def test_homepage_url_is_exist(self):
		response = Client().get('/homepage/')
		self.assertEqual(response.status_code, 200)
		
	def test_home_url_is_notexist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_kegiatanku_function(self):
		response = resolve('/homepage/')
		self.assertEqual(response.func, homepage)

	def test_landingPage_using_landingPage_template(self):
		response = Client().get('/homepage/')
		self.assertTemplateUsed(response, 'homepage.html')

	# def test_landing_page_is_completed(self):
	# 	request = HttpRequest()
	# 	response = homepage(request)
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn('Halo, apa kabar?', html_response)

	def test_landing_page_title_is_right(self):
		request = HttpRequest()
		response = homepage(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>About Me</title>', html_response)

	#Redirect landing page

	def test_landing_page_redirecting(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)
